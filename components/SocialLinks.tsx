import { TwitterIcon } from '@icons/TwitterIcon'
import { FacebookIcon } from '@icons/FacebookIcon'
import { SocialRss } from '@components/SocialRss'
import { GhostSettings } from '@lib/ghost'
import { InstagramIcon } from './icons/InstagramIcon'
import { TiktokIcon } from '@icons/TiktokIcon'

interface SocialLinkProps {
  siteUrl: string
  site: GhostSettings
}

export const SocialLinks = ({ siteUrl, site }: SocialLinkProps) => {
  const twitterUrl = site.twitter && `https://twitter.com/${site.twitter.replace(/^@/, ``)}`
  const facebookUrl = site.facebook && `https://www.facebook.com/${site.facebook.replace(/^\//, ``)}`

  const { processEnv } = site
  const { memberSubscriptions } = processEnv

  return (
    <>
      {site.facebook && (
        <a href={facebookUrl} className="social-link social-link-fb" target="_blank" rel="noopener noreferrer" title="Facebook">
          <FacebookIcon />
        </a>
      )}
      <a href={"https://vt.tiktok.com/ZSJEo2X3q/"} className="social-link social-link-icon" target="_blank" rel="noopener noreferrer" title="Facebook">
        <TiktokIcon />
      </a>
      <a href={"https://www.instagram.com/_ne_duck_/"} className="social-link social-link-icon" target="_blank" rel="noopener noreferrer" title="Facebook">
        <InstagramIcon />
      </a>
      {/* {site.twitter && (
        <a href={twitterUrl} className="social-link social-link-tw" target="_blank" rel="noopener noreferrer" title="Twitter">
          <TwitterIcon />
        </a>
      )} */}
      {!memberSubscriptions && <SocialRss {...{ siteUrl }} />}
    </>
  )
}
